package ee.bcs.valiit.liis;

import ee.bcs.valiit.liis.model.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LiisApp {

    static List<Inimene> inimesed = new ArrayList<>();

    public static void main(String[] args) {
        try {
            DateFormat format = new SimpleDateFormat("dd.MM.yyyy");
            Date synnikuupaev = format.parse("21.12.1972");
            Inimene inim1 = new Inimene("Ülari", "Ainjärv", synnikuupaev);
            inim1.lisaJuhiloaKategooria(JuhiloaKategooria.A);
            inim1.lisaJuhiloaKategooria(JuhiloaKategooria.C);
            inimesed.add(inim1);
            Inimene inim2 = new Inimene("Katja", "Sobo", format.parse("01.01.1970"));
            inim2.lisaJuhiloaKategooria(JuhiloaKategooria.T);
            inimesed.add(inim2);

            Rula rula = new Rula(LiikumisVahendImp.generateId());
            Ratas ratas = new Ratas(LiikumisVahendImp.generateId());
            Auto auto = new Auto(LiikumisVahendImp.generateId());
            auto.setAutoTyyp(AutoTyyp.MANGUAUTO);
            auto.setRegNumber("063 TKL");
            auto.setNoutudJuhiloaKategooria(null);
            Veesoiduk veesoiduk = new Veesoiduk(LiikumisVahendImp.generateId());
            veesoiduk.setVeesoidukiTyyp(VeesoidukiTyyp.PURJEKAS);

            rula.soidab("Õismäelt", "Kesklinna");
            ratas.soidab("Kesklinnast", "Kadriorgu");
            auto.soidab("Kadriorust", "Piritale");
            veesoiduk.soidab("Piritalt", "Tilgule");

            for (Inimene inimene : inimesed) {
                System.out.print(inimene.getEesnimi() + " " + inimene.getPerenimi() + ", " + format.format(inimene.getSynnikuupaev()) + ": ");
                for (JuhiloaKategooria kat : inimene.getJuhiloaKategooriad()) {
                    System.out.print(kat + " ");
                }
                System.out.println();
            }
        } catch (ParseException e) {
            System.err.println(e.getMessage());
        } catch (Exception e) {
            System.err.println("Error!");
        } finally {
            System.out.println("Kõik on läbi!");
        }
    }

}
