package ee.bcs.valiit.liis.model;

import lombok.Data;

@Data
public class Auto extends Mootorsoiduk {

    private AutoTyyp autoTyyp;

    public Auto(Long id) {
        super(id);
        setNoutudJuhiloaKategooria(JuhiloaKategooria.B);
    }

    @Override
    public void soidab(String algus, String lopp) {
        boolean kasLaksKaima = kaivitub();
        if (kasLaksKaima) {
            super.soidab(algus, lopp);
            boolean kasJaiSeisma = seiskub();
            if (kasJaiSeisma) {
                return;
            } else {
                System.err.println("Töötava mootoriga autot pole ilus maha jätta!");
            }
        } else {
            System.err.println("Otsi teine auto!");
        }
    }

}
