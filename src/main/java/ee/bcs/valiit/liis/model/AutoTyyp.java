package ee.bcs.valiit.liis.model;

public enum AutoTyyp {
    SOIDUAUTO, TAKSO, VEAAUTO, KAUBIK, MANGUAUTO
}
