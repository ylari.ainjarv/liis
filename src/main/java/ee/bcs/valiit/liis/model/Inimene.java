package ee.bcs.valiit.liis.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class Inimene {

    private String eesnimi;

    private String perenimi;

    private Date synnikuupaev;

    private List<JuhiloaKategooria> juhiloaKategooriad;

    public Inimene() {
        juhiloaKategooriad = new ArrayList<>();
    }

    public Inimene(String eesnimi, String perenimi, Date synnikuupaev) {
        this.eesnimi = eesnimi;
        this.perenimi = perenimi;
        this.synnikuupaev = synnikuupaev;
        juhiloaKategooriad = new ArrayList<>();
    }

    public List<JuhiloaKategooria> lisaJuhiloaKategooria(JuhiloaKategooria juhiloaKategooria) {
        juhiloaKategooriad.add(juhiloaKategooria);
        return juhiloaKategooriad;
    }

}
