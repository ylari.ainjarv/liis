package ee.bcs.valiit.liis.model;

import lombok.Data;

import java.util.concurrent.ThreadLocalRandom;

@Data
public class LiikumisVahendImp implements LiikumisVahendInt {

    private Long id;

    public LiikumisVahendImp(Long id) {
        this.id = id;
    }

    @Override
    public void soidab(String algus, String lopp) {
        System.out.println(id + " sõidab " + algus + " kuni " + lopp);
    }

    public static Long generateId() {
        return ThreadLocalRandom.current().nextLong();
    }

}
