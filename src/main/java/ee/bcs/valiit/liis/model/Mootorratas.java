package ee.bcs.valiit.liis.model;

public class Mootorratas extends Mootorsoiduk {
    public Mootorratas(Long id) {
        super(id);
        setNoutudJuhiloaKategooria(JuhiloaKategooria.A);
    }
}
