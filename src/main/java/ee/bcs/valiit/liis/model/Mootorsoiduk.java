package ee.bcs.valiit.liis.model;

import lombok.Data;

@Data
public class Mootorsoiduk extends LiikumisVahendImp {

    private String regNumber;

    private JuhiloaKategooria noutudJuhiloaKategooria;

    public boolean kaivitub() {
        System.out.println(getId() + " läks käima ...");
        return true;
    }

    public boolean seiskub() {
        System.out.println(getId() + " jäi seisma ...");
        return true;
    }

    public Mootorsoiduk(Long id) {
        super(id);
    }

}
