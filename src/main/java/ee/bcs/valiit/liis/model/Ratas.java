package ee.bcs.valiit.liis.model;

import lombok.Data;

@Data
public class Ratas extends LiikumisVahendImp {

    private Integer kaikudeArv;

    private IstumisAsend istumisAsend;

    private String seeriaNr;

    public Ratas(Long id) {
        super(id);
    }

}
