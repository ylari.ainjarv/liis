package ee.bcs.valiit.liis.model;

import lombok.Data;

@Data
public class Veesoiduk extends Mootorsoiduk {

    private VeesoidukiTyyp veesoidukiTyyp;

    public Veesoiduk(Long id) {
        super(id);
    }
}
