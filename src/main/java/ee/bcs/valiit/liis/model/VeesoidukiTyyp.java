package ee.bcs.valiit.liis.model;

public enum VeesoidukiTyyp {
    PAAT, LAEV, JAHT, PURJEKAS
}
